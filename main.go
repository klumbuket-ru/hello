package main

import "fmt"

const defaultVersion = "1.0.0"

// App приложение
type App struct {
	Version string
}

func main() {
	var app = &App{
		Version: defaultVersion,
	}

	fmt.Println("Hello...")
	fmt.Println("Version:", app.Version)
}
